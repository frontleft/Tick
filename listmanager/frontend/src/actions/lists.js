import { GET_LISTS, DELETE_LIST, ADD_LIST } from './types'

// Get Lists
export const getLists = () => dispatch => {
  fetch('/api/lists/')
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: GET_LISTS,
        payload: data
      })
    }).catch(err => {
      console.error(err)
    })
}

// Delete List
export const deleteList = id => dispatch => {
  fetch(`/api/lists/${id}`, {
    method: 'DELETE'
  })
    .then(res => {
      dispatch({
        type: DELETE_LIST,
        payload: id
      })
    }).catch(err => {
      console.error(err)
    })
}

// Add List
export const addList = list => dispatch => {
  console.log('add list')
  fetch('api/lists/', {
    method: 'POST',
    body: JSON.stringify(list),
    headers: {
      'Content-Type': 'application/json'
    }
  })
    .then(res => res.json())
    .then(data => {
      dispatch({
        type: ADD_LIST,
        payload: data
      })
    })
    .catch(err => console.error(err))
}
