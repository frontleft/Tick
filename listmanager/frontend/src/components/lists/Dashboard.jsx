import React, { Fragment } from 'react';
import Form from './Form';
import Lists from './Lists';


const Dashboard = () => {

  return (
    <Fragment>
      <Form />
      <Lists />
    </Fragment>
  )
}

export default Dashboard
