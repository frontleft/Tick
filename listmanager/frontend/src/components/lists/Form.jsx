import React, { useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { addList } from '../../actions/lists';

const Form = ({addList}) => {
  const propTypes = {
    addList: PropTypes.func.isRequired
  }

  const [values, setValues] = useState({
    name: '',
    description: ''
  })

  const handleChange = name => (event) => {
    setValues({ ...values, [name]: event.target.value })
  }

  const handleSubmit = e => {
    e.preventDefault()
    const { name, description } = values;
    const myList = { name, description }
    addList(myList)
    setValues({
      name: '',
      description: ''
    })
  }

  return (
    <div>
      <h2>Add a List</h2>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Name</label>
          <input name="name" type="text" value={values.name} onChange={handleChange('name')} />
        </div>
        <div className="form-group">
          <label>Description</label>
          <input name="description" type="text" value={values.description} onChange={handleChange('description')} />
        </div>
        <div className="form-group">
          <button onClick={handleSubmit}>Submit</button>
        </div>
      </form>
    </div>
  )
}

export default connect(null, { addList })(Form);
