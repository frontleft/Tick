import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getLists, deleteList } from '../../actions/lists';

const Lists = ({getLists, deleteList, lists}) => {
  const propTypes = {
    lists: PropTypes.array.isRequired,
    getLists: PropTypes.func.isRequired,
    deleteList: PropTypes.func.isRequired
  }

  useEffect(() => {
    getLists()
  }, [])

  return (
    <Fragment>
      <h2>Tick Lists</h2>
      <table>
        <thead> 
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
            <th /> 
          </tr>
        </thead>
        <tbody>
          {lists.length > 0 ? lists.map(list => (
            <tr key={list.id}>
              <td>{list.id}</td>
              <td>{list.name}</td>
              <td>{list.description}</td>
              <td><button onClick={() => deleteList(list.id)}>Delete</button></td>
            </tr>
          )) : null}
        </tbody>
      </table>
    </Fragment>
  )
}

const mapStateToProps = state => ({
  lists: state.lists.lists
})


export default connect(mapStateToProps, { getLists, deleteList })(Lists);
