import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import store from '../store';

import Header from './layouts/Header';
import Dashboard from './lists/Dashboard';

const alertOptions = {
  timeout: 3000,
  position: 'top center',
}

const App = () => (
  <Provider store={store}>
      <Fragment>
        <Header />
        <div>
          <Dashboard />
        </div>
      </Fragment>
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('app'));