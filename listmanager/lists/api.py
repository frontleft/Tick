from lists.models import List
from rest_framework import viewsets, permissions

from .serializers import ListSerializer

# Lead Viewset
class ListViewSet(viewsets.ModelViewSet):
    # queryset = List.objects.all()
    permission_classes = [
        permissions.IsAuthenticated
    ] 

    serializer_class = ListSerializer

    def get_queryset(self):
        return self.request.user.leads.all()
    
    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
        