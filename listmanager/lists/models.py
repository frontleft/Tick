from django.db import models
from django.contrib.auth.models import User


class List(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    owner = models.ForeignKey(User,
                              related_name="lists",
                              on_delete=models.CASCADE,
                              null=True)
    created_at = models.DateTimeField(auto_now_add=True)